import { h } from 'preact'
import { Router } from 'preact-router'

import Header from './components/header'

// Code-splitting is automated for `routes` directory
import Home from './pages/home'
import Profile from './pages/profile'

const App = () => (
  <div id="app">
    <Header />
    <main>
      <Router>
        <Home path="/" />
        <Profile path="/profile/" user="me" />
        <Profile path="/profile/:user" />
      </Router>
    </main>
  </div>
)

export default App
