import { h } from 'preact'
import style from './style.module.scss'
import { Button } from 'antd'

const Home = () => {
  return (
    <div className={style.home}>
      <a href="https://qiuqian.xyz">
        <img
          src="../../assets/preact-logo.svg"
          alt="Preact Logo"
          height="160"
          width="160"
        />
      </a>
      <h1>picbed</h1>
      <Button type="primary" size="large">
        antd test button
      </Button>
    </div>
  )
}

export default Home
