# picbed

## 文档
  - [Preact](https://preactjs.com/guide/v10/getting-started)
  - [Antd](https://ant-design.antgroup.com/components/overview-cn)
  - [OSS](https://help.aliyun.com/zh/oss/developer-reference/getting-started-with-oss-sdk-for-node-js?spm=a2c4g.11186623.0.i4)

## 环境
  - yarn
  - react
  - node version >=16.0.0

## 项目启动
  1. cd picbed
  2. yarn
  3. yarn dev

## 搭建todo
  - sso
  - alioss
    - node_modules
    - api
  - sass
  - processenv
  - eslint
  - precommit
  - gitlab ci
  - nginx
  - domain



